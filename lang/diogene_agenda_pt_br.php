<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/diogene_agenda?lang_cible=pt_br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'evenement_supprimer' => 'Excluir o evento',

	// F
	'form_legend_agenda' => 'Evento',

	// L
	'label_agenda_caches' => 'Campos da agenda não exibidos',
	'label_agenda_legende' => 'Legenda da parte "evento" do formulário',
	'label_agenda_multiple' => 'Permitir a vinculação de vários eventos a uma matéria',
	'label_agenda_obligatoire' => 'O evento é obrigatório',
	'label_cfg_ajout_agenda' => 'Evento',

	// T
	'titre_evenements_lies' => 'Eventos vinculados'
);
