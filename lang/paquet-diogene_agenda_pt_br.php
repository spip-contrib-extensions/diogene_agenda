<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-diogene_agenda?lang_cible=pt_br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'diogene_agenda_description' => 'Inclui a possibilidade de vincular um evento a uma matéria a partir do seu próprio formulário.',
	'diogene_agenda_nom' => 'Diogenes - Agenda',
	'diogene_agenda_slogan' => 'Complemento agenda para o "Diogenes"'
);
