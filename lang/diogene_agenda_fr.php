<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/diogene_agenda.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'evenement_supprimer' => 'Supprimer l’événement',

	// F
	'form_legend_agenda' => 'Événement',

	// L
	'label_agenda_caches' => 'Champs de l’agenda à ne pas afficher',
	'label_agenda_legende' => 'Légende de la partie "événement" du formulaire',
	'label_agenda_multiple' => 'Permettre de lier plusieurs événements à un article',
	'label_agenda_obligatoire' => 'L’événement est obligatoire',
	'label_cfg_ajout_agenda' => 'Événement',

	// T
	'titre_evenements_lies' => 'Événements liés'
);
