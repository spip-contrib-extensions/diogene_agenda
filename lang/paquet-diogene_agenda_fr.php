<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/diogene_agenda.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'diogene_agenda_description' => 'Ajoute la possibilité d’ajouter un évènement à un article depuis son propre formulaire.',
	'diogene_agenda_nom' => 'Diogène - Agenda',
	'diogene_agenda_slogan' => 'Complément agenda pour "Diogène"'
);
